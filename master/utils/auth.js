var knex=require('../config/database_connection');
var express = require('express');
var router = express.Router();

const auth=function(req,res,next){
    let headers=req.headers;
    let query=req.query;
    let token=headers.token || query.token;
    console.log("Token:"+token);
    if(!token){
        return res.status(401).json({
            message:"Token required"
        })
    }
knex
.select()
.from('token_info')
.where({
    api_token:token
})
.then(function(data){
    if(!data || !data.length){
        return res.json({
            message:"Wrong Token"
        })
    }
    next();
})
.catch(function(err){
    res.json({
        status:"ERROR",
        error:err
    })
});
}

module.exports=auth;