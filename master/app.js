var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');


var indexRouter = require('./routes/index');
var countryRouter=require('./routes/country');
var countryDatabaseRouter=require('./routes/country_database');
var tokenRouter=require('./routes/token')
var movieRouter=require('./routes/movies');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json({limit:'50mb'}));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());


app.use('/', indexRouter);
app.use('/api/country', countryRouter);
app.use('/database', countryDatabaseRouter);
app.use('/token', tokenRouter);
app.use('/movies',movieRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.status(404).json({
    status:"ERROR",
    message:"API NOT FOUND",
    err:err.message
  }); 
});

module.exports = app;
