var auth = require('../utils/auth')
var knex = require('../config/database_connection');
var redis = require('../config/redis');
var async = require('async');
var express = require('express');
var router = express.Router();
const HOST=process.env.HOST || 'localhost';




router.get('/', function (req, res, next) {
  res.json({
    status: "SUCCESS",
    message: "Movies API",
    list: `http://${HOST}:3000/movies/list`,
    lookup: `http://${HOST}:3000/movies/lookup/query?column_name=&key=&token=`,
    for_inserting_post:`http://${HOST}:3000/movies/insert`

  });
});

//used only once to crate table in database
router.get('/create', function (req, res) {
  knex.schema.createTable('movies_info', function (table) {
      table.increments();
      table.string("name");
      table.string("release_date");
      table.string("director_name");
      table.string("genre");

    })
    .then(function (data) {
      res.json("Table created")
    })
    .catch(function (err) {
      res.json(err);
    })
})

router.post('/insert',auth, function (req, res) {
  let body = req.body;
  let m_name = body.name;
  let m_director_name = body.director_name;
  console.log(body.director_name)
  knex
    .select()
    .from('movies_info')
    .where({
      name: m_name,
      director_name: m_director_name
    })
    .then(function (data) {
      if (data && data != "") {
        console.log(data)
        res.json({
          message: "movie already present and add one movie at a time"
        })
      } else {
        knex
          .insert(body)
          .into('movies_info')
          .then(function (data) {
            res.json({
              message: "Movie Inserted"
            })
          })
          .catch(function (err) {
            res.json({
              message: "Problem In Inserting Movie Data"
            })

          })
      }

    })
    .catch(function (err) {
      res.status(500).json({
        error: err.message
      })
    })

})

router.get('/list',auth, function (req, res) {
  knex
    .select()
    .from('movies_info')
    .then(function (list) {
      res.json({
        status: "SUCCESS",
        message: "Dispalying list of Movies",
        res: list
      })
    })
    .catch(function (err) {
      res.status(500).json({
        message: "Problem in dispalying Movie list",
        error: err
      })
    })
})

router.get('/lookup/query',auth, function (req, res) {
  const key = req.query.key && req.query.key.toString();
  const column_name = req.query.column_name && req.query.column_name.toString();

  const redis_key = column_name + "_" + key.toLowerCase().trim().split("").join("_");
  redis.get(redis_key, function (err, data) {
    if (err) {
      res.status(500).json({
        message: "Failed to connect to redis",
        error: err.message
      })
    }
    if (data) {
      data = JSON.parse(data);
      return res.json({
        status: "SUCCESS",
        message: "Displaying list from cache",
        res: data

      })
    }
    knex
      .select()
      .from('movies_info')
      .where({
        [column_name]: key
      })
      .then(function (list) {
        redis.set(redis_key, JSON.stringify(list || []));
        res.json({
          status: "SUCCESS",
          message: "Displaying list from database",
          res: list
        })
      })
      .catch(function (err) {
        res.status(500).json({
          message: "Problem in dispalying list",
          error: err.message
        })
      })

  })
})



module.exports = router;