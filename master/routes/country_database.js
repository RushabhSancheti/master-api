var auth = require('../utils/auth');
var knex = require('../config/database_connection');
var redis = require('../config/redis');
var async = require('async');
var express = require('express');
var router = express.Router();
const Country = require('../Countries/country')
const HOST=process.env.HOST || 'localhost';




router.get('/', function (req, res, next) {
  res.json({
    status: "SUCCESS",
    message: "COUNTRY API",
    list: `http://${HOST}:3000/database/list`,
    lookup:`http://${HOST}:3000/database/lookup/param`,
    lookup1:`http://${HOST}:3000/database/lookup/query?column_name=&key=`
  });
});

//used only once to crate table in database
router.get('/create', function (req, res) {
  knex.schema.createTable('country_info', function (table) {
      table.increments();
      table.string("country");
      table.string("calling_code");
      table.string("population");
      table.string("capital_city");
      table.string("currency_code");
      table.string("currency_name");
      table.string("tld");
      table.string("religion");
      table.string("continent");
      table.string("temperature");
      table.string("government");
      table.string("independence");
      table.string("region");
      table.string("languages");
      table.string("abbreviation");
      table.string("iso");
      table.string("area");
      table.string("density");
      table.timestamps();
    })
    .then(function (data) {
      res.json("Table created")
    })
    .catch(function (err) {
      res.json(err);
    })
});
//used to insert all the country data in database  
router.post('/insert', function (req, res) {
  let body = req.body;
  body = body.map(function (item) {
    const data = {
      ...item,
      languages: item.languages && item.languages.join(",") || ""
    }
    return data
    //console.log(body);
  });
  const queue = async.queue(function (item, cb) {
    knex
      .insert(item)
      .into('country_info')
      .then(function (data) {
        cb(null, data)
      })
      .catch(function (err) {
        console.log(err)
        cb(err);
      })
  }, 15)
  queue.drain(function () {
    res.json({
      message: "Data inserted"
    })
  })
  queue.error(function () {
    res.json({
      message: "Data not inserted",
      error:err
    })
  })
  queue.push(body)
});
router.get('/list', auth, function (req, res) {
  knex
    .select('country', 'calling_code', 'population', 'languages')
    .from('country_info')
    .then(function (list) {
      res.json({
        status: "SUCCESS",
        message: "Dispalying list from database",
        res: list
      })
    })
    .catch(function (err) {
      res.status(500).json({
        message: "Problem in dispalying list",
        error: err
      })
    })
})
//used to lookup country based on capital_city
router.get('/lookup/param/:key', auth, function (req, res) {
  const key = req.params.key || req.params.key.toString();
  //
  knex
    .select('country', 'calling_code', 'currency_code', 'currency_name', 'population', 'region', 'continent', 'government')
    .from('country_info')
    .where({
      capital_city: key
    })
    .then(function (list) {
      res.json({
        status: "SUCCESS",
        message: "Displaying list from database",
        res: list
      })
    })
    .catch(function (err) {
      res.status(500).json({
        message: "Problem in dispalying list",
        error: err
      })
    })
})

router.get('/lookup/query', auth, function (req, res) {
  const key = req.query.key && req.query.key.toString();
  const column_name = req.query.column_name || req.query.column_name.toString();

  const redis_key = column_name + "_" + key.toLowerCase().trim().split("").join("_");
  redis.get(redis_key, function (err, data) {
    if (err) {
     return res.status(500).json({
        message: "Failed to connect to redis",
        error: err.message
      });
    }
    if (data) {
      data = JSON.parse(data);
      return res.json({
        status: "SUCCESS",
        message: "Displaying list from cache",
        res: data

      })
    }
    let columns = ['country', 'calling_code', 'continent', 'population']
    if (column_name && key) {
      if (!columns.includes('column_name'))
        columns.push(column_name);
    }
    knex
      .select(columns)
      .from('country_info')
      .where({
        [column_name]: key
      })
      .then(function (list) {
        redis.set(redis_key, JSON.stringify(list || []));
        res.json({
          status: "SUCCESS",
          message: "Displaying list from database",
          res: list
        })
      })
      .catch(function (err) {
        res.status(500).json({
          message: "Problem in dispalying list",
          error: err.message
        })
      })
  })

 

});

module.exports = router;