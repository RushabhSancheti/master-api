var express = require('express');
var router = express.Router();
const HOST=process.env.HOST || "localhost";

/* GET home page. */
router.get('/', function (req, res, next) {
  res.json({
    status: "SUCCESS",
    message: "COUNTRY API",
    static_data:`http://${HOST}:3000/api/country`,
    database:`http://${HOST}:3000/database`,
    movie:`http://${HOST}:3000/movies`,
    post_for_genrating_token:`http://${HOST}:3000/token/genrate/api_token`

  });
});

module.exports = router;