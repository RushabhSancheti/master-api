var async = require('async');
var express = require('express');
var router = express.Router();
const Country = require('../Countries/country');
const HOST=process.env.HOST || 'localhost';



const CObj = {};
const CountryFilteredObj = {};
const ContinentObj = {};
const ContinentFilteredObj = {};
const RegionObj = {};
const RegionFilteredObj = {};

Country.forEach(function (item) {
  if (item.abbreviation) {
    CObj[item.capital_city] = item;
    CountryFilteredObj[item.capital_city.toString().toUpperCase()] = {
      country: item.country,
      capital: item.capital_city,
      continent: item.continent,
      population: item.population,
      calling_code: item.calling_code,
      doamin_tld: item.tld,
      currency_code: item.currency_code
    }
  }
  if (item.continent) {
    if (ContinentObj[item.continent]) {
      ContinentObj[item.continent].push(item);
    } else {
      ContinentObj[item.continent] = [item];
    }
    if (ContinentFilteredObj[item.continent.toString().toUpperCase()]) {
      ContinentFilteredObj[item.continent.toString().toUpperCase()].push({
        country: item.country,
        capital: item.capital_city,
        continent: item.continent,
        population: item.population,
        calling_code: item.calling_code,
        doamin_tld: item.tld,
        currency_code: item.currency_code
      })
    } else {
      ContinentFilteredObj[item.continent.toString().toUpperCase()] = [{
        country: item.country,
        capital: item.capital_city,
        continent: item.continent,
        population: item.population,
        calling_code: item.calling_code,
        doamin_tld: item.tld,
        currency_code: item.currency_code
      }]
    }

  }
  if (item.region) {
    if (RegionObj[item.region]) {
      RegionObj[item.region].push(item)
    } else {
      RegionObj[item.region] = [item];
    }
    if (RegionFilteredObj[item.region.toString().toUpperCase()]) {
      RegionFilteredObj[item.region.toString().toUpperCase()].push({
        country: item.country,
        capital: item.capital_city,
        continent: item.continent,
        calling_code: item.calling_code,
        doamin_tld: item.tld,
        region: item.region

      })
    } else {
      RegionFilteredObj[item.region.toString().toUpperCase()] = [{
        country: item.country,
        capital: item.capital_city,
        continent: item.continent,
        calling_code: item.calling_code,
        doamin_tld: item.tld,
        region: item.region

      }]
    }
  }

})
//console.log(RegionFilteredObj)


const countryList = Country.map((m) => {
  return {
    country: m.country,
    capital: m.capital_city,
    continent: m.continent,
    population: m.population
  }
})

router.get('/', function (req, res, next) {
  res.json({
    status: "SUCCESS",
    message: "COUNTRY API",
    list: `http://${HOST}:3000/api/country/list`,
    lookup:`http://${HOST}:3000/api/country/lookup/query?capital_name=`,
    lookup:`http://${HOST}:3000/api/country/lookup/param/`
  });
});

// To display the list of countries
router.get('/list', function (req, res, next) {
  res.json({
    status: "SUCCESS",
    message: "COUNTRY API",
    res: countryList
  });
});


//For lookup using query string
router.get('/lookup/query', function (req, res) {
  const capital = req.query.capital_city && req.query.capital_city.toString().toUpperCase();
  console.log(capital);
  let country = CountryFilteredObj;
  if (capital) {
    country = country[capital] || {};
  }

  res.json({
    status: "SUCCESS",
    message: "Here is the country you are looking for",
    res: country
  })
});

//To find country by passing  query parameter
router.get('/lookup/param/:key', (req, res) => {
  const key = req.params.key && req.params.key.toString().toUpperCase();
  console.log(key);
  let resp = CountryFilteredObj;
  if (key) {
    resp = CountryFilteredObj[key];
    if (!resp) {
      console.log("Continent")
      resp = ContinentFilteredObj[key];
    }
    if (!resp) {
      console.log("Hi")
      resp = RegionFilteredObj[key] || {};
    }
  }
  res.json({
    status: "SUCESS",
    mesaage: "",
    res: resp
  })

})


module.exports = router;