var hat = require('hat');
var knex = require('../config/database_connection');
var redis = require('../config/redis');
var async = require('async');
var express = require('express');
var router = express.Router()
router.get('/create/token_table', function (req, res) {
    knex.schema.createTable('token_info', function (table) {
            table.increments();
            table.string("email");
            table.string("api_token");
            table.timestamps();
        })
        .then(function (data) {
            res.json({
                message: "Table created"
            })
        })
        .catch(function (err) {
            res.json({
                message: "Table is not created",
                error: err
            })
        })
});

router.post('/genrate/api_token', function (req, res) {
    const body = req.body;
    if (!body.email) {
        return res.json({
            message: "Email required"
        })
    }

    knex
        .select()
        .from('token_info')
        .where({
            email: body.email
        })
        .then(function (data) {
            if (data && data.length) {
                console.log("email:" + data.email)
                return res.json({
                    status: "SUCCESS",
                    message: "Token is already present",
                    res: {
                        email: data[0].email,
                        api_token: data[0].api_token
                    }
                })
            }

            const info = {
                email: body.email,
                api_token: hat()
            }

            knex('token_info')
                .insert(info)
                .then(function (data) {
                    res.json({
                        status: "SUCCESS",
                        message: "Token created Succesfully"
                    })

                })
                .catch(function (error) {
                    res.json({
                        status: "Error",
                        message: "Datbase error"
                    })

                })
        })
        .catch(function (err) {
            res.json({
                status: "Error",
                message: "Failed to create token",
                res: err
            })

        })

})

module.exports = router;